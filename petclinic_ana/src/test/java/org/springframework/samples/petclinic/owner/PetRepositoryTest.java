package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTest {
	
	@Autowired
	private PetRepository pets;
	
	@Autowired
	private OwnerRepository owners;
	
	private Pet pet;
	private Owner owner;
	
	
	@Before
	public void init() {
		//Collection <Vet> cVets = 
		if (this.pet==null) {
			pet = new Pet ();	
			this.owner = new Owner();
			this.owner.setFirstName("Ana");
			this.owner.setLastName("Castaño");
			this.owner.setAddress("Calle");
			this.owner.setCity("Caceres");
			this.owner.setTelephone("888");
			owners.save(this.owner);
		pet.setName("sena");
		pet.setBirthDate(LocalDate.now());
		pet.setComments("guapa");
		pet.setOwner(owner);
		pet.setType(pets.findPetTypes().get(0));
		pet.setWeight(31);
		pets.save(pet);
			
		}
		
		
	}
	
	@Test
	@Transactional
	public void metodo() {
		assertNotNull(pets.findById(pet.getId()));
		pets.delete(pet);
		assertNull(pets.findById(pet.getId()));
		
	}
	
	
	}
	

