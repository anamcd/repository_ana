package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;


public class VisitTests {
	
	public static Vet vet;
	public static Visit visit;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		vet = new Vet();
		visit = new Visit();
		vet.addVisit(visit);
		visit.setDate(LocalDate.now());
		visit.setDescription("surgery");
		visit.setVet(vet);
	}
	
	@Test
	public void testVet() {
		assertNotNull(vet);				
	}
	@Test
	public void testVisit() {
		assertNotNull(visit);				
	}

	@Test
	public void testVisits() {
		assertNotNull(vet.getVisits());	
		assertEquals(vet.getVisits().size(), 1 );
		assertEquals(visit.getDate(), LocalDate.now());
		assertEquals(visit.getDescription(), "surgery");
		assertEquals(visit.getVet(), vet);
	}

    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet = null;
    	visit = null;
    }

}