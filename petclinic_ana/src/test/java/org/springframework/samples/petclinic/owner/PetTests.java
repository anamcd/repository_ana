
package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class PetTests {
	
	public static Pet pet;
	public static Pet pet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		pet = new Pet();
		pet.setWeight(31);
		pet.setComments("Raza chow chow");
	}

	@Before
	public void testPet() {
		assertNotNull(pet);				
	}

	
	@Test
	public void testWeight() {
		assertNotNull(pet.getWeight());
		assertNotEquals(pet.getWeight(), 15);
		assertEquals((int)pet.getWeight(), 31);		
	}
	
	@Test
	public void testComments() {
		assertNotNull(pet.getComments());
		assertEquals(pet.getComments(), "Raza chow chow");
		assertNotEquals(pet.getComments(), "Raza caniche");		
	}
	
    @Test
    public void testSerialization() {
    	Pet pet3 = new  Pet();
    	pet3.setWeight(20);
		pet3.setComments("Raza yorkshire");
        
        Pet other = (Pet) SerializationUtils
                .deserialize(SerializationUtils.serialize(pet3));
        assertThat(other.getWeight()).isEqualTo(pet3.getWeight());
        assertThat(other.getComments()).isEqualTo(pet3.getComments());
    }
    
	@Ignore
	//This test is not executed
	public void ignore() {
		pet.setWeight(15);
	}
    

    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	pet = null;
    }
    

}